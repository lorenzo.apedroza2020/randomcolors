//301018@LorenzoPedroza & Chenfei Yan

package com.randomcolors.l2.lpedr.randomcolors;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private View windowView;
    private Button tryMeButton;
    private int[] colors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        colors = new int[]{Color.CYAN, Color.GREEN, Color.RED, Color.BLUE,
                Color.BLACK, Color.DKGRAY, Color.LTGRAY, Color.MAGENTA, Color.YELLOW};

        windowView = findViewById(R.id.windowViewId);


        tryMeButton = findViewById(R.id.tryMeButton);

        tryMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Random random = new Random();
                int randomNumber = random.nextInt(colors.length);

                windowView.setBackgroundColor(colors[randomNumber]);

                Log.d("TEST", "Tap");
                Log.d("RANDOM", String.valueOf(randomNumber));

            }
        });





    }

}
